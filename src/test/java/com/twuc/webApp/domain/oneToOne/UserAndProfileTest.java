package com.twuc.webApp.domain.oneToOne;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class UserAndProfileTest extends JpaTestBase {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private UserProfileEntityRepository userProfileEntityRepository;

    @Test
    void should_save_user_and_profile() {
        // TODO
        //
        // 请书写一个测试实现如下的功能：
        //
        // Given 一个 UserEntity 和一个 UserProfileEntity。它们并没有持久化。
        // When 持久化 UserEntity
        // Then UserProfileEntity 也一同被持久化了。
        //
        // <--start--
        UserEntity userEntity = new UserEntity();
        UserProfileEntity userProfileEntity = new UserProfileEntity("xiao", "ming");
        userProfileEntity.setUserEntity(userEntity);
        userEntity.setUserProfileEntity(userProfileEntity);
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(entityManager -> {
            UserEntity entity = userEntityRepository.save(userEntity);
            expectedId.setValue(entity.getId());
        });

        run(entityManager -> {
            UserEntity entity = userEntityRepository.findById(expectedId.getValue()).get();
            assertEquals(1L, entity.getId().longValue());
            assertEquals("xiao", entity.getUserProfileEntity().getFirstName());
            assertEquals(1, userProfileEntityRepository.findAll().size());
        });
        // --end->
    }

    @Test
    void should_remove_parent_and_child() {
        // TODO
        //
        // 请书写一个测试：
        //
        // Given 一个持久化了的 UserEntity 和 UserProfileEntity
        // When 删除 UserEntity
        // Then UserProfileEntity 也被删除了
        //
        // <--start-
        UserEntity userEntity = new UserEntity();
        UserProfileEntity userProfileEntity = new UserProfileEntity("xiao", "ming");
        userProfileEntity.setUserEntity(userEntity);
        userEntity.setUserProfileEntity(userProfileEntity);
        ClosureValue<Long> expectedId = new ClosureValue<>();

        flushAndClear(entityManager -> {
            UserEntity entity = userEntityRepository.save(userEntity);
            expectedId.setValue(entity.getId());
        });

        flush(entityManager -> {
            userEntityRepository.deleteById(expectedId.getValue());
        });

        run(entityManager -> {
            boolean present = userEntityRepository.findById(expectedId.getValue()).isPresent();
            assertFalse(present);
            assertEquals(0, userProfileEntityRepository.findAll().size());
        });
        // --end->
    }
}